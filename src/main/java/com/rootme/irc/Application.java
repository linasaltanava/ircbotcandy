package com.rootme.irc;

import com.rootme.irc.client.RootMeChallengeListener;
import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;

public class Application {
    public final static String CHANNEL = "#root-me_challenge";

    public static void main(String[] args) {
        Configuration configuration = new Configuration.Builder()
                .setName("candy")
                .setAutoNickChange(true)
                .setLogin("candy")
                .addServer("irc.root-me.org", 6667)
                .addAutoJoinChannel(CHANNEL)
                .addListener(new RootMeChallengeListener())
                .buildConfiguration();

        PircBotX candy = new PircBotX(configuration);
        try {
            candy.startBot();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
