package com.rootme.irc.client;

import org.pircbotx.User;
import org.pircbotx.UserHostmask;

public class CustomUser extends User {
    protected CustomUser(UserHostmask hostmask) {
        super(hostmask);
    }

    @Override
    public void setNick(String nick) {
        super.setNick(nick);
    }
}
