package com.rootme.irc.client;

import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.JoinEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;

public class RootMeChallengeListener extends ListenerAdapter {
    private CustomUser customUser;

    @Override
    public void onJoin(JoinEvent event) {
        if (customUser == null) {
            customUser = new CustomUser(event.getUserHostmask());
            customUser.setNick("Candy");
        }

        customUser.send().message("!ep1");
    }

    @Override
    public void onPrivateMessage(PrivateMessageEvent event) {
        String msg = event.getMessage();
        if (msg.contains("/")) {
            String[] numbers = event.getMessage().split("/");

            double a = Double.valueOf(numbers[0]);
            double b = Double.valueOf(numbers[1]);

            customUser.send().message("!ep1 -rep " + f(a, b));
        }
    }

    private double f(double a, double b) {
        return Math.round(Math.sqrt(a) * b * 100.0) / 100.0;
    }
}
